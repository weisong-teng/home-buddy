import { Account } from "@/models/account";
import { IAlert } from "@/models/alert";
import { InjectionKey } from "vue";
import { createStore, Store } from "vuex";
import createPersistedState from "vuex-persistedstate";

export interface State {
  account: Account;
  alert: IAlert;
  isBurgerMenuActive: boolean;
  isPageReady: boolean;
}

export const key: InjectionKey<Store<State>> = Symbol();

export const store: Store<State> = createStore<State>({
  state: {
    account: {} as Account,
    alert: {} as IAlert,
    isBurgerMenuActive: false,
    isPageReady: true,
  },
  mutations: {
    setAccount(state, payload: Account) {
      state.account = payload;
    },
    setAlert(state, payload: IAlert) {
      state.alert = payload;
    },
    toggleAlert(state) {
      state.alert.show = !state.alert.show;
    },
    showBurgerMenu(state) {
      state.isBurgerMenuActive = true;
    },
    hideBurgerMenu(state) {
      state.isBurgerMenuActive = false;
    },
    setPageReady(state, isPageReady: boolean) {
      state.isPageReady = isPageReady;
    },
  },
  actions: {},
  getters: {},
  plugins: [createPersistedState()],
});
