import { IHomeSimple } from "./home";

export interface IResidentSimple {
  id: string;
  isOwner: boolean;
  username: string;
  accountId: string;
}

export interface IResidentDetail {
  id: string;
  isOwner: boolean;
  home: IHomeSimple;
}
