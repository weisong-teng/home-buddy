import { IHomeSimple } from "./home";
import { IResidentSimple } from "./resident";

export interface IPaymentCategory {
  id: string;
  name: string;
  budget: number;
  order: number;
}

export interface IPaymentCategoryCreate {
  name: string;
  budget: number;
  order: number;
}

export interface IPaymentCategoryUpdate {
  id: string;
  name: string;
  budget: number;
  order: number;
}

export interface IPaymentSimple {
  id: string;
  name: string;
  dateTime: Date;
  amount: number;
  category?: IPaymentCategory;
  resident: IResidentSimple;
}

export interface IPaymentDetail {
  id: string;
  name: string;
  dateTime: string;
  amount: number;
  remarks: string;
  category?: IPaymentCategory;
  resident: IResidentSimple;
  home: IHomeSimple;
}

export interface IPaymentCreate {
  name: string;
  dateTime: string;
  amount: number;
  categoryId: string;
  remarks: string;
  residentId: string;
  homeId: string;
}

export interface IPaymentUpdate {
  id: string;
  name: string;
  dateTime: string;
  amount: number;
  categoryId: string;
  residentId: string;
  remarks: string;
}

export interface IPaymentStats {
  residents: Array<IResidentSimple>;
  categorisedBreakdown: Array<{
    category: string;
    amount: number;
    budget: number;
  }>;
  categorisedTrend: Array<{
    category: string;
    trendItems: Array<{
      year: string;
      month: string;
      amount: number;
    }>;
  }>;
}
