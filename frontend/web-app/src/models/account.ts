import { IResidentDetail } from "@/models/resident";

export interface IAccountLogin {
  username: string;
  password: string;
}

export interface IAccountRegister {
  username: string;
  email: string;
  password: string;
}

export interface IAccountPasswordReset {
  accountId: string,
  token: string;
  password: string;
}

export interface IAccountAccess {
  accessToken: string;
  audience: string;
  issuer: string;
  expireAt: Date;
  id: string;
  username: string;
  role: string;
}

export interface IAccountInfo {
  id: string;
  username: string;
  email: string;
  role: string;
  residents: Array<IResidentDetail>;
}

export interface IAccountSimple {
  id: string;
  username: string;
}

export class Account implements IAccountAccess, IAccountInfo {
  accessToken: string;
  audience: string;
  issuer: string;
  expireAt: Date;
  id: string;
  username: string;
  email: string;
  role: string;
  residents: Array<IResidentDetail> = new Array<IResidentDetail>();

  constructor(accessToken: string) {
    const tokenBody = JSON.parse(atob(accessToken.split(".")[1]));

    this.accessToken = accessToken;
    this.audience = tokenBody.aud;
    this.issuer = tokenBody.iss;
    this.expireAt = new Date(tokenBody.exp * 1000);
    this.id = tokenBody.sub;
    this.username =
      tokenBody["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"];
    this.role =
      tokenBody["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
    this.email = "";
  }

  addAccountInfo(accountInfo: IAccountInfo): void {
    this.email = accountInfo.email;
    this.residents = accountInfo.residents;
  }
}
