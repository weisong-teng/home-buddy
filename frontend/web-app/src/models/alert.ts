export interface IAlert {
  type: AlertType;
  message: string;
  show: boolean;
}

export enum AlertType {
  success,
  warning,
}

export class Alert implements IAlert {
  type: AlertType;
  message: string;
  show: boolean;

  constructor(alertType = AlertType.success, message = "", show = true) {
    this.type = alertType;
    this.message = message;
    this.show = show;
  }
}
