import {
  IPaymentCategory,
  IPaymentCategoryCreate,
  IPaymentCategoryUpdate,
  IPaymentSimple,
} from "./payment";
import { IResidentSimple } from "./resident";

export interface IHomeSimple {
  id: string;
  name: string;
  address: string;
  postalCode: string;
  picture: string;
  ownerAccountIds: Array<string>;
}

export interface IHomeDetail {
  id: string;
  name: string;
  address: string;
  postalCode: string;
  picture: string;
  residents: Array<IResidentSimple>;
  payments: Array<IPaymentSimple>;
  paymentCategories: Array<IPaymentCategory>;
}

export interface IHomeCreate {
  name: string;
  address: string;
  postalCode: string;
  picture: string;
}

export interface IHomeUpdate {
  id: string;
  name: string;
  address: string;
  postalCode: string;
  picture: string;
  accountsToAdd: Array<string>;
  residentsToRemove: Array<string>;
  residentsToUpgrade: Array<string>;
  residentsToDowngrade: Array<string>;
  paymentCategoriesToCreate: Array<IPaymentCategoryCreate>;
  paymentCategoriesToUpdate: Array<IPaymentCategoryUpdate>;
  paymentCategoriesToDelete: Array<string>;
}

export interface IHomeDetailForPaymentSetup {
  id: string;
  name: string;
  residents: Array<IResidentSimple>;
  paymentCategories: Array<IPaymentCategory>;
}
