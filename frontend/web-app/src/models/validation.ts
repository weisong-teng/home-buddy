import Joi from "joi";

export interface IValidationError {
  path: string;
  message: string;
}

export class ValidationResult {
  validationErrors: Array<IValidationError> = [];

  public set(joiValidationResult: Joi.ValidationResult): void {
    this.validationErrors = [];

    if (joiValidationResult.error)
      for (const item of joiValidationResult.error.details) {
        this.validationErrors.push({
          path: item.path[0].toString(),
          message: item.message,
        });
      }
  }

  public hasError(path: string | null = null): boolean {
    if (path === null) return this.validationErrors.length > 0;
    return this.validationErrors.find((e) => e.path === path) != null;
  }

  public getErrorMessage(path: string): string {
    const index = this.validationErrors.findIndex((e) => e.path === path);

    return index === -1 ? "error" : this.validationErrors[index].message;
  }

  public clear(): void {
    this.validationErrors = [];
  }
}
