import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Dashboard from "@/views/Dashboard.vue";
import Homes from "../views/Homes.vue";
import HomeDetails from "../views/HomeDetails.vue";
import HomeCreate from "../views/HomeCreate.vue";
import HomeUpdate from "../views/HomeUpdate.vue";
import HomePayments from "../views/HomePayments.vue";
import PaymentCreate from "../views/PaymentCreate.vue";
import PaymentUpdate from "../views/PaymentUpdate.vue";
import SignUp from "@/views/SignUp.vue";
import LogIn from "@/views/LogIn.vue";
import Account from "@/views/Account.vue";
import ResetPassword from "@/views/ResetPassword.vue";
import ResetPasswordEmail from "@/views/ResetPasswordEmail.vue";

import { store } from "@/store";
import moment from "moment-timezone";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/homes",
    name: "Homes",
    component: Homes,
  },
  {
    path: "/homes/:id",
    name: "HomeDetails",
    component: HomeDetails,
  },
  {
    path: "/homes/edit/:id",
    name: "HomeUpdate",
    component: HomeUpdate,
  },
  {
    path: "/homes/new",
    name: "HomeCreate",
    component: HomeCreate,
  },
  {
    path: "/homes/:id/payments",
    name: "HomePayments",
    component: HomePayments,
  },
  {
    path: "/payments/new/:id",
    name: "PaymentCreate",
    component: PaymentCreate,
  },
  {
    path: "/payments/edit/:id",
    name: "PaymentUpdate",
    component: PaymentUpdate,
  },
  {
    path: "/signup",
    name: "SignUp",
    component: SignUp,
  },
  {
    path: "/login",
    name: "LogIn",
    component: LogIn,
  },
  {
    path: "/account",
    name: "Account",
    component: Account,
  },
  {
    path: "/resetPassword",
    name: "ResetPassword",
    component: ResetPassword,
  },
  {
    path: "/resetPasswordEmail",
    name: "ResetPasswordEmail",
    component: ResetPasswordEmail,
  },
  {
    path: "/",
    name: "Dashboard",
    component: Dashboard,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  const publicPages = [
    "Dashboard",
    "LogIn",
    "SignUp",
    "ResetPassword",
    "ResetPasswordEmail",
  ];

  const isLoggedIn =
    store.state.account.accessToken &&
    moment(store.state.account.expireAt).isSameOrAfter(moment.utc());

  if (!publicPages.includes(to.name as string) && !isLoggedIn)
    next({ name: "LogIn" });
  else if (to.path == "/signup" && isLoggedIn) next({ name: "Dashboard" });
  else next();
});

export default router;
