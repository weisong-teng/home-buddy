using HomeBuddy.Backend.Models;

namespace HomeBuddy.Backend.Persistence
{
    public interface IHomeRepository
    {
        void CreateHome(Home home);
        void DeleteHome(Home home);
        Task<Home?> GetHome(Guid id);
        Task<IEnumerable<Home>> GetHomes(string? accountId = null);
        void UpdateHome(Home home);
    }
}