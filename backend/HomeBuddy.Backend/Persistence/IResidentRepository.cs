using HomeBuddy.Backend.Models;

namespace HomeBuddy.Backend.Persistence
{
    public interface IResidentRepository
    {
        void CreateResident(Resident resident);
        void DeleteResident(Resident resident);
        Task<Resident?> GetResident(Guid id);
        Task<IEnumerable<Resident>> GetResidents();
        Task<IEnumerable<Resident>> GetResidentsByAccount(Account account);
        void UpdateResident(Resident resident);
    }
}