namespace HomeBuddy.Backend.Persistence
{
    public interface IUnitOfWork
    {
        Task SaveChanges();
    }
}