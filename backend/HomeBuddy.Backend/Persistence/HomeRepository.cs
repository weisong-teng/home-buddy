using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeBuddy.Backend.Extensions;
using HomeBuddy.Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace HomeBuddy.Backend.Persistence
{
    public class HomeRepository : IHomeRepository
    {
        private readonly BackendDbContext _dbContext;

        public HomeRepository(BackendDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Home>> GetHomes(string? accountId = null)
        {
            if (_dbContext.Homes == null)
                return new List<Home>();

            if (accountId == null)
                return await _dbContext.Homes
                    .Include(h => h.Residents.Where(r => r.IsOwner))
                    .ToListAsync();

            return await _dbContext.Homes
                .Where(h => h.Residents.Any(r => r.AccountId == accountId))
                .Include(h => h.Residents.Where(r => r.IsOwner))
                .ToListAsync();
        }

        public async Task<Home?> GetHome(Guid id)
        {
            if (_dbContext.Homes == null)
                return null;
            var now = DateTime.UtcNow.GetDateTimeInTimeZone("Asia/Singapore");
            var year = now.Year;
            var month = now.Month;

            var from = TimeZoneInfo.ConvertTimeToUtc(new DateTime(year, month, 1), TimeZoneInfo.FindSystemTimeZoneById("Asia/Singapore"));
            var to = TimeZoneInfo.ConvertTimeToUtc(new DateTime(year, month, DateTime.DaysInMonth(year, month), 23, 59, 59, 999), TimeZoneInfo.FindSystemTimeZoneById("Asia/Singapore"));

            return await _dbContext
                .Homes
                .Include(h => h.Residents)
                .ThenInclude(r => r.Account)
                .Include(h => h.PaymentCategories)
                .Include(h => h.Payments
                    .Where(p => p.DateTime.ToUniversalTime() >= from)
                    .Where(p => p.DateTime.ToUniversalTime() <= to)
                    .OrderByDescending(p => p.DateTime))
                .ThenInclude(p => p.Category)
                .SingleOrDefaultAsync(h => h.Id == id);
        }

        public void CreateHome(Home home)
        {
            _dbContext.Add<Home>(home);
        }

        public void UpdateHome(Home home)
        {
            _dbContext.Entry<Home>(home).State = EntityState.Modified;
        }

        public void DeleteHome(Home home)
        {
            _dbContext.Remove<Home>(home);
        }
    }
}