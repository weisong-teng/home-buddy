using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeBuddy.Backend.Extensions;
using HomeBuddy.Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace HomeBuddy.Backend.Persistence
{
    public class PaymentRepository : IPaymentRepository
    {
        private readonly BackendDbContext _dbContext;

        public PaymentRepository(BackendDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Payment>> GetPayments()
        {
            if (_dbContext.Payments == null)
                return new List<Payment>();

            return await _dbContext
                .Payments
                .Include(p => p.Category)
                .Include(p => p.Resident)
                .ThenInclude(r => r.Account)
                .OrderByDescending(p => p.DateTime)
                .ToListAsync();
        }

        public async Task<IEnumerable<Payment>> GetPaymentsByHome(Guid homeId)
        {
            if (_dbContext.Payments == null)
                return new List<Payment>();

            return await _dbContext
                .Payments
                .Where(p => p.HomeId == homeId)
                .Include(p => p.Category)
                .Include(p => p.Resident)
                .ThenInclude(r => r.Account)
                .OrderByDescending(p => p.DateTime)
                .ToListAsync();
        }

        public async Task<IEnumerable<Payment>> GetPaymentsByHome(Guid homeId, int year, int month)
        {
            if (_dbContext.Payments == null)
                return new List<Payment>();

            return await _dbContext
                .Payments
                .Where(p => p.DateTime.ToUniversalTime().Year == year)
                .Where(p => p.DateTime.ToUniversalTime().Month == month)
                .Where(p => p.HomeId == homeId)
                .Include(p => p.Category)
                .Include(p => p.Resident)
                .ThenInclude(r => r.Account)
                .OrderByDescending(p => p.DateTime)
                .ToListAsync();
        }

        public async Task<IEnumerable<Payment>> GetPaymentsByHome(Guid homeId, DateTime from, DateTime to)
        {
            if (_dbContext.Payments == null)
                return new List<Payment>();

            return await _dbContext
                .Payments
                .Where(p => p.DateTime.ToUniversalTime() >= from)
                .Where(p => p.DateTime.ToUniversalTime() <= to)
                .Where(p => p.HomeId == homeId)
                .Include(p => p.Category)
                .Include(p => p.Resident)
                .ThenInclude(r => r.Account)
                .OrderByDescending(p => p.DateTime)
                .ToListAsync();
        }

        public async Task<Payment?> GetPayment(Guid id)
        {
            if (_dbContext.Payments == null)
                return null;

            return await _dbContext
                .Payments
                .Include(p => p.Category)
                .Include(p => p.Resident)
                .Include(p => p.Home)
                .SingleOrDefaultAsync(p => p.Id == id);
        }

        public void CreatePayment(Payment payment)
        {
            _dbContext.Add<Payment>(payment);
        }

        public void UpdatePayment(Payment payment)
        {
            _dbContext.Entry<Payment>(payment).State = EntityState.Modified;
        }

        public void DeletePayment(Payment payment)
        {
            _dbContext.Remove<Payment>(payment);
        }
        public async Task<IEnumerable<PaymentCategory>> GetPaymentCategories()
        {
            if (_dbContext.PaymentCategories == null)
                return new List<PaymentCategory>();

            return await _dbContext
                .PaymentCategories
                .OrderBy(pc => pc.Order)
                .ToListAsync();
        }

        public async Task<PaymentCategory?> GetPaymentCategory(Guid id)
        {
            if (_dbContext.PaymentCategories == null)
                return null;

            return await _dbContext
                .PaymentCategories
                .SingleOrDefaultAsync(pc => pc.Id == id);
        }

        public void CreatePaymentCategory(PaymentCategory paymentCategory)
        {
            _dbContext.Add<PaymentCategory>(paymentCategory);
        }

        public void UpdatePaymentCategory(PaymentCategory paymentCategory)
        {
            _dbContext.Entry<PaymentCategory>(paymentCategory).State = EntityState.Modified;
        }

        public void DeletePaymentCategory(PaymentCategory paymentCategory)
        {
            _dbContext.Entry(paymentCategory).Collection(pc => pc.Payments).Load();
            _dbContext.Remove<PaymentCategory>(paymentCategory);
        }
    }
}