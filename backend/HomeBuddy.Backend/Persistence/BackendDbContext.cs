using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeBuddy.Backend.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace HomeBuddy.Backend.Persistence
{
    public class BackendDbContext : IdentityDbContext<Account>
    {
        public DbSet<Home>? Homes { get; set; }
        public DbSet<Resident>? Residents { get; set; }
        public DbSet<Payment>? Payments { get; set; }
        public DbSet<PaymentCategory>? PaymentCategories { get; set; }

        public BackendDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            SeedData(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        private void SeedData(ModelBuilder modelBuilder)
        {
            var passwordHasher = new PasswordHasher<Account>();
            var accounts = new List<Account>();
            accounts.Add(new Account()
            {
                Id = "8e445865-a24d-4543-a6c6-9443d048cdb9",
                UserName = "admin1",
                NormalizedUserName = "admin1".ToUpper(),
                Email = "weisong.teng.work+admin1@gmail.com",
                NormalizedEmail = "weisong.teng.work+admin1@gmail.com".ToUpper(),
                PasswordHash = passwordHasher.HashPassword(null, "Password!234")
            });
            accounts.Add(new Account()
            {
                Id = "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                UserName = "user1",
                NormalizedUserName = "user1".ToUpper(),
                Email = "weisong.teng.work+user1@gmail.com",
                NormalizedEmail = "weisong.teng.work+user1@gmail.com".ToUpper(),
                PasswordHash = passwordHasher.HashPassword(null, "Password!234")
            });
            accounts.Add(new Account()
            {
                Id = "f22bdbe5-bf78-407e-908f-2fbb034e6c3d",
                UserName = "user2",
                NormalizedUserName = "user2".ToUpper(),
                Email = "weisong.teng.work+user2@gmail.com",
                NormalizedEmail = "weisong.teng.work+user2@gmail.com".ToUpper(),
                PasswordHash = passwordHasher.HashPassword(null, "Password!234")
            });

            var roles = new List<IdentityRole>();
            roles.Add(new IdentityRole()
            {
                Id = "2c5e174e-3b0e-446f-86af-483d56fd7210",
                Name = "Admin",
                NormalizedName = "admin".ToUpper()
            });
            roles.Add(new IdentityRole()
            {
                Id = "72953f5a-f149-4ef4-bb9e-cfc09975e868",
                Name = "User",
                NormalizedName = "user".ToUpper()
            });

            var identityUserRoles = new List<IdentityUserRole<string>>();
            identityUserRoles.Add(new IdentityUserRole<string>()
            {
                RoleId = "2c5e174e-3b0e-446f-86af-483d56fd7210",
                UserId = "8e445865-a24d-4543-a6c6-9443d048cdb9"
            });
            identityUserRoles.Add(new IdentityUserRole<string>()
            {
                RoleId = "72953f5a-f149-4ef4-bb9e-cfc09975e868",
                UserId = "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3"
            });
            identityUserRoles.Add(new IdentityUserRole<string>()
            {
                RoleId = "72953f5a-f149-4ef4-bb9e-cfc09975e868",
                UserId = "f22bdbe5-bf78-407e-908f-2fbb034e6c3d"
            });

            var paymentCategories = new List<PaymentCategory>();
            paymentCategories.Add(new PaymentCategory()
            {
                Id = new Guid("39497e24-20ff-4990-811e-aaf494084b9f"),
                Name = "Rental",
                HomeId = new Guid("32a58340-a987-421b-9f66-a6db89f55777")
            });
            paymentCategories.Add(new PaymentCategory()
            {
                Id = new Guid("16ca6564-a12e-4858-b78a-8dd0ceec2266"),
                Name = "Grocery",
                HomeId = new Guid("32a58340-a987-421b-9f66-a6db89f55777")
            });
            paymentCategories.Add(new PaymentCategory()
            {
                Id = new Guid("5902c45d-0dc4-488a-8bff-2457c3868014"),
                Name = "Furniture",
                HomeId = new Guid("32a58340-a987-421b-9f66-a6db89f55777")
            });
            paymentCategories.Add(new PaymentCategory()
            {
                Id = new Guid("fd1ef8e1-9864-4214-a9e2-9a7f1ec86962"),
                Name = "Utilities",
                HomeId = new Guid("32a58340-a987-421b-9f66-a6db89f55777")
            });
            paymentCategories.Add(new PaymentCategory()
            {
                Id = new Guid("3948af5c-1d5c-4eca-8ef1-7b4502490dc8"),
                Name = "Broadband",
                HomeId = new Guid("32a58340-a987-421b-9f66-a6db89f55777")
            });
            paymentCategories.Add(new PaymentCategory()
            {
                Id = new Guid("6a0ba162-0767-4fd8-b1f5-d4f9f780f6ca"),
                Name = "Other",
                HomeId = new Guid("32a58340-a987-421b-9f66-a6db89f55777")
            });

            var homes = new List<Home>();
            homes.Add(new Home()
            {
                Id = new Guid("32a58340-a987-421b-9f66-a6db89f55777"),
                Name = "Home 1",
                Address = "Block 1 Street 1 #01-23",
                PostalCode = "123456"
            });
            homes.Add(new Home()
            {
                Id = new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"),
                Name = "Home 2",
                Address = "No 2 Street 12",
                PostalCode = "008605"
            });

            var payments = new List<Payment>();
            payments.Add(new Payment()
            {
                Id = new Guid("108e7c96-ea21-44f2-9cbe-db4237c2d1dd"),
                Name = "Payment 1",
                DateTime = (new DateTime(2021, 1, 5, 23, 0, 0)).ToUniversalTime(),
                Amount = 10,
                CategoryId = new Guid("39497e24-20ff-4990-811e-aaf494084b9f"),
                HomeId = new Guid("32a58340-a987-421b-9f66-a6db89f55777"),
                ResidentId = new Guid("884d167d-b30c-49fb-b30e-254bece11bf5")
            });
            payments.Add(new Payment()
            {
                Id = new Guid("8e445865-a24d-4543-a6c6-9443d048cdb9"),
                Name = "Payment 2",
                DateTime = (new DateTime(2021, 2, 14, 10, 0, 0)).ToUniversalTime(),
                Amount = 20,
                CategoryId = new Guid("16ca6564-a12e-4858-b78a-8dd0ceec2266"),
                HomeId = new Guid("32a58340-a987-421b-9f66-a6db89f55777"),
                ResidentId = new Guid("884d167d-b30c-49fb-b30e-254bece11bf5")
            });
            payments.Add(new Payment()
            {
                Id = new Guid("41881c20-df28-43df-8e1c-e42748181ea3"),
                Name = "Payment 1",
                DateTime = (new DateTime(2021, 7, 9, 11, 0, 0)).ToUniversalTime(),
                Amount = new decimal(21.03),
                CategoryId = new Guid("16ca6564-a12e-4858-b78a-8dd0ceec2266"),
                HomeId = new Guid("32a58340-a987-421b-9f66-a6db89f55777"),
                ResidentId = new Guid("884d167d-b30c-49fb-b30e-254bece11bf5")
            });

            var residents = new List<Resident>();
            residents.Add(new Resident()
            {
                Id = new Guid("884d167d-b30c-49fb-b30e-254bece11bf5"),
                HomeId = new Guid("32a58340-a987-421b-9f66-a6db89f55777"),
                AccountId = "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3"
            });
            residents.Add(new Resident()
            {
                Id = new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"),
                HomeId = new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"),
                AccountId = "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3"

            });
            residents.Add(new Resident()
            {
                Id = new Guid("1790bfca-812a-40ba-ac29-fe8b3e1b6836"),
                HomeId = new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"),
                AccountId = "f22bdbe5-bf78-407e-908f-2fbb034e6c3d"
            });

            modelBuilder.Entity<Home>().HasData(homes);
            modelBuilder.Entity<Resident>().HasData(residents);
            modelBuilder.Entity<Payment>().HasData(payments);
            modelBuilder.Entity<PaymentCategory>().HasData(paymentCategories);
            modelBuilder.Entity<Account>().HasData(accounts);
            modelBuilder.Entity<IdentityRole>().HasData(roles);
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(identityUserRoles);
        }
    }
}