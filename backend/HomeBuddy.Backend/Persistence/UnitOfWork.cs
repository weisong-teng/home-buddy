using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BackendDbContext _dbContext;

        public UnitOfWork(BackendDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task SaveChanges()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}