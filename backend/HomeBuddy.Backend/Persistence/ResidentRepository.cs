using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeBuddy.Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace HomeBuddy.Backend.Persistence
{
    public class ResidentRepository : IResidentRepository
    {
        private readonly BackendDbContext _dbContext;

        public ResidentRepository(BackendDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Resident>> GetResidents()
        {
            if (_dbContext.Residents == null)
                return new List<Resident>();

            return await _dbContext
                .Residents
                .ToListAsync();
        }

        public async Task<IEnumerable<Resident>> GetResidentsByAccount(Account account)
        {
            if (_dbContext.Residents == null)
                return new List<Resident>();

            return await _dbContext
                .Residents
                .Include(r => r.Home)
                .Where(r => r.Account == account)
                .ToListAsync();
        }

        public async Task<Resident?> GetResident(Guid id)
        {
            if (_dbContext.Residents == null)
                return null;

            return await _dbContext
                .Residents
                .Include(r => r.Home)
                .SingleOrDefaultAsync(p => p.Id == id);
        }

        public void CreateResident(Resident resident)
        {
            _dbContext.Add<Resident>(resident);
        }

        public void UpdateResident(Resident resident)
        {
            _dbContext.Entry<Resident>(resident).State = EntityState.Modified;
        }

        public void DeleteResident(Resident resident)
        {
            _dbContext.Remove<Resident>(resident);
        }
    }
}