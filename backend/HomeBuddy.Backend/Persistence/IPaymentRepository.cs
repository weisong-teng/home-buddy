using HomeBuddy.Backend.Models;

namespace HomeBuddy.Backend.Persistence
{
    public interface IPaymentRepository
    {
        Task<IEnumerable<Payment>> GetPayments();
        Task<IEnumerable<Payment>> GetPaymentsByHome(Guid homeId);
        Task<IEnumerable<Payment>> GetPaymentsByHome(Guid homeId, int year, int month);
        Task<IEnumerable<Payment>> GetPaymentsByHome(Guid homeId, DateTime from, DateTime to);
        Task<Payment?> GetPayment(Guid id);
        Task<IEnumerable<PaymentCategory>> GetPaymentCategories();
        Task<PaymentCategory?> GetPaymentCategory(Guid id);
        void CreatePaymentCategory(PaymentCategory paymentCategory);
        void UpdatePaymentCategory(PaymentCategory paymentCategory);
        void DeletePaymentCategory(PaymentCategory paymentCategory);
        void CreatePayment(Payment payment);
        void UpdatePayment(Payment payment);
        void DeletePayment(Payment payment);
    }
}