using HomeBuddy.Backend.Models;
using Microsoft.AspNetCore.Identity;

namespace HomeBuddy.Backend.Services
{
    public interface IAccountService
    {
        Task<IList<Account>> GetAccounts(string role = "User");
        Task<string?> Authenticate(Account account, string password);
        Task<IdentityResult> CreateAccount(Account account, string password, bool isAdmin);
        Task<Account> GetAccount(string userName);
        Task<Account> GetAccountById(string accountId);
        Task<Account> GetAccountByEmail(string email);
        Task<IList<string>> GetRoles(Account account);
        Task<string> GeneratePasswordResetToken(Account account);
        Task<IdentityResult> ResetPassword(Account account, string token, string password);
    }
}