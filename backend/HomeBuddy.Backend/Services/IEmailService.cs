using HomeBuddy.Backend.Models;

namespace HomeBuddy.Backend.Services
{
    public interface IEmailService
    {
        Task<bool> SendEmail(EmailRequest emailRequest);
    }
}