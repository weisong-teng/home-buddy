using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeBuddy.Backend.Models;
using HomeBuddy.Backend.Options;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace HomeBuddy.Backend.Services
{
    public class EmailService : IEmailService
    {
        private readonly SendGridOptions _sendGridOptions;

        public EmailService(IOptions<SendGridOptions> sendGridOptionsAccessor)
        {
            _sendGridOptions = sendGridOptionsAccessor.Value;
        }

        public async Task<bool> SendEmail(EmailRequest emailRequest)
        {
            var client = new SendGridClient(_sendGridOptions.ApiKey);

            string templateId;
            switch (emailRequest.EmailPurpose)
            {
                case EmailPurpose.ResetPassword:
                    templateId = _sendGridOptions.ConfirmPasswordResetTemplateId;
                    break;
                default:
                    templateId = string.Empty;
                    break;
            }

            var message = new SendGridMessage()
            {
                From = new EmailAddress(_sendGridOptions.SenderEmail, _sendGridOptions.SenderName),
                TemplateId = templateId
            };
            message.SetTemplateData(new { callbackUrl = emailRequest.CallbackUrl });
            message.AddTos(emailRequest.Recipients.Select(r => new EmailAddress(r)).ToList());

            var response = await client.SendEmailAsync(message);

            return response.IsSuccessStatusCode;
        }
    }
}