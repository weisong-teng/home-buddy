using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using HomeBuddy.Backend.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace HomeBuddy.Backend.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<Account> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;

        public AccountService(UserManager<Account> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
        }

        public async Task<IList<Account>> GetAccounts(string role = "User")
        {
            return await _userManager.GetUsersInRoleAsync(role);
        }

        public async Task<Account> GetAccount(string userName)
        {
            return await _userManager.FindByNameAsync(userName);
        }

        public async Task<Account> GetAccountById(string accountId)
        {
            return await _userManager.FindByIdAsync(accountId);
        }

        public async Task<Account> GetAccountByEmail(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<IList<string>> GetRoles(Account account)
        {
            return await _userManager.GetRolesAsync(account);
        }

        public async Task<IdentityResult> CreateAccount(Account account, string password, bool isAdmin)
        {
            var accountCreated = await _userManager.CreateAsync(account, password);

            if (isAdmin)
                accountCreated = await _userManager.AddToRoleAsync(account, "Admin");
            else
                accountCreated = await _userManager.AddToRoleAsync(account, "User");

            return accountCreated;
        }

        public async Task<string?> Authenticate(Account account, string password)
        {
            var isPasswordValid = await _userManager.CheckPasswordAsync(account, password);
            if (!isPasswordValid)
                return null;

            var roles = await _userManager.GetRolesAsync(account);

            var claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, account.Id));
            claims.Add(new Claim(ClaimTypes.Name, account.UserName));
            foreach (var role in roles)
                claims.Add(new Claim(ClaimTypes.Role, role));

            var audience = _configuration.GetSection("Authentication:Audience").Get<string>();
            var issuer = _configuration.GetSection("Authentication:Issuer").Get<string>();
            var secretKey = _configuration.GetSection("Authentication:SecretKey").Get<string>();

            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));

            var token = new JwtSecurityToken(issuer: issuer, audience: audience, expires: DateTime.Now.AddHours(1), claims: claims, signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256));
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<string> GeneratePasswordResetToken(Account account)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(account);
        }

        public async Task<IdentityResult> ResetPassword(Account account, string token, string password)
        {
            return await _userManager.ResetPasswordAsync(account, token, password);
        }
    }
}