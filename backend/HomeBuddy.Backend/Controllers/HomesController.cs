using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using HomeBuddy.Backend.Models;
using HomeBuddy.Backend.Models.DTOs;
using HomeBuddy.Backend.Persistence;
using HomeBuddy.Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HomeBuddy.Backend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class HomesController : ControllerBase
    {
        private readonly IHomeRepository _homeRepository;
        private readonly IResidentRepository _residentRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IAccountService _accountService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public HomesController(IHomeRepository homeRepository, IResidentRepository residentRepository, IPaymentRepository paymentRepository, IAccountService accountService, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _homeRepository = homeRepository;
            _residentRepository = residentRepository;
            _paymentRepository = paymentRepository;
            _accountService = accountService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetHomes()
        {
            var accountId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (accountId == null)
                return BadRequest();

            var homes = _mapper.Map<IList<HomeSimple>>(await _homeRepository.GetHomes(accountId));

            return Ok(homes);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetHome(Guid id)
        {
            var home = _mapper.Map<HomeDetail>(await _homeRepository.GetHome(id));

            if (home == null)
                return NotFound();

            home.PaymentCategories = home.PaymentCategories?.OrderBy(pc => pc.Order);

            return Ok(home);
        }

        [HttpGet("{id}/forpaymentsetup")]
        public async Task<IActionResult> GetHomeForPaymentSetup(Guid id)
        {
            var home = _mapper.Map<HomeDetailForPaymentSetup>(await _homeRepository.GetHome(id));

            if (home == null)
                return NotFound();

            home.PaymentCategories = home.PaymentCategories?.OrderBy(pc => pc.Order);

            return Ok(home);
        }

        [HttpPost]
        public async Task<IActionResult> CreateHome(HomeCreate homeCreate)
        {
            var accountId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (accountId == null)
                return BadRequest();

            var account = await _accountService.GetAccountById(accountId);
            if (account == null)
                return NotFound();

            var home = _mapper.Map<Home>(homeCreate);
            _homeRepository.CreateHome(_mapper.Map<Home>(home));

            var resident = new Resident(account, home);
            resident.IsOwner = true;
            _residentRepository.CreateResident(resident);

            await _unitOfWork.SaveChanges();

            return CreatedAtAction(nameof(GetHome), new { Id = home.Id }, _mapper.Map<HomeDetail>(home));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateHome(Guid id, HomeUpdate homeUpdate)
        {
            if (id != homeUpdate.Id)
                return BadRequest();

            var home = _mapper.Map<Home>(homeUpdate);
            _homeRepository.UpdateHome(home);

            if (homeUpdate.AccountsToAdd.Count() > 0)
                foreach (string accountId in homeUpdate.AccountsToAdd)
                {
                    var account = await _accountService.GetAccountById(accountId);
                    if (account != null)
                    {
                        var resident = new Resident(account, home);
                        _residentRepository.CreateResident(resident);
                    }
                }

            if (homeUpdate.ResidentsToRemove.Count() > 0)
                foreach (Guid residentId in homeUpdate.ResidentsToRemove)
                {
                    var resident = await _residentRepository.GetResident(residentId);
                    if (resident != null)
                        _residentRepository.DeleteResident(resident);
                }

            if (homeUpdate.ResidentsToUpgrade.Count() > 0)
                foreach (Guid residentId in homeUpdate.ResidentsToUpgrade)
                {
                    var resident = await _residentRepository.GetResident(residentId);
                    if (resident != null)
                    {
                        resident.IsOwner = true;
                        _residentRepository.UpdateResident(resident);
                    }
                }

            if (homeUpdate.ResidentsToDowngrade.Count() > 0)
                foreach (Guid residentId in homeUpdate.ResidentsToDowngrade)
                {
                    var resident = await _residentRepository.GetResident(residentId);
                    if (resident != null)
                    {
                        resident.IsOwner = false;
                        _residentRepository.UpdateResident(resident);
                    }
                }

            if (homeUpdate.PaymentCategoriesToCreate.Count() > 0)
                foreach (PaymentCategory paymentCategory in _mapper.Map<List<PaymentCategory>>(homeUpdate.PaymentCategoriesToCreate))
                {
                    if (paymentCategory != null)
                    {
                        paymentCategory.Home = home;
                        _paymentRepository.CreatePaymentCategory(paymentCategory);
                    }
                }

            if (homeUpdate.PaymentCategoriesToUpdate.Count() > 0)
                foreach (var paymentCategoryUpdate in homeUpdate.PaymentCategoriesToUpdate)
                {
                    var paymentCategory = await _paymentRepository.GetPaymentCategory(paymentCategoryUpdate.Id);
                    if (paymentCategory != null)
                    {
                        paymentCategory.Name = paymentCategoryUpdate.Name;
                        paymentCategory.Budget = paymentCategoryUpdate.Budget;
                        paymentCategory.Order = paymentCategoryUpdate.Order;
                        _paymentRepository.UpdatePaymentCategory(paymentCategory);
                    }
                }

            if (homeUpdate.PaymentCategoriesToDelete.Count() > 0)
                foreach (Guid paymentCategoryId in homeUpdate.PaymentCategoriesToDelete)
                {
                    var paymentCategory = await _paymentRepository.GetPaymentCategory(paymentCategoryId);
                    if (paymentCategory != null)
                        _paymentRepository.DeletePaymentCategory(paymentCategory);
                }

            await _unitOfWork.SaveChanges();

            _homeRepository.UpdateHome(home);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHome(Guid id)
        {
            var home = await _homeRepository.GetHome(id);

            if (home == null)
                return NotFound();

            _homeRepository.DeleteHome(home);

            await _unitOfWork.SaveChanges();

            return NoContent();
        }
    }
}