using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HomeBuddy.Backend.Extensions;
using HomeBuddy.Backend.Models;
using HomeBuddy.Backend.Models.DTOs;
using HomeBuddy.Backend.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HomeBuddy.Backend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PaymentsController : ControllerBase
    {
        private readonly IPaymentRepository _paymentRepository;
        private readonly IHomeRepository _homeRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IResidentRepository _residentRepository;

        public PaymentsController(IPaymentRepository paymentRepository, IHomeRepository homeRepository, IResidentRepository residentRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _paymentRepository = paymentRepository;
            _homeRepository = homeRepository;
            _residentRepository = residentRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetPayments(Guid? homeId, int? year, int? month)
        {
            IEnumerable<Payment> payments;

            if (homeId == null)
            {
                payments = await _paymentRepository.GetPayments();
            }
            else
            {
                var home = await _homeRepository.GetHome(homeId.Value);
                if (home == null)
                    return NotFound();

                if (year == null || month == null)
                {
                    year = DateTime.UtcNow.GetDateTimeInTimeZone("Asia/Singapore").Year;
                    month = DateTime.UtcNow.GetDateTimeInTimeZone("Asia/Singapore").Month;
                }

                var from = TimeZoneInfo.ConvertTimeToUtc(new DateTime(year.Value, month.Value, 1), TimeZoneInfo.FindSystemTimeZoneById("Asia/Singapore"));
                var to = TimeZoneInfo.ConvertTimeToUtc(new DateTime(year.Value, month.Value, DateTime.DaysInMonth(year.Value, month.Value), 23, 59, 59, 999), TimeZoneInfo.FindSystemTimeZoneById("Asia/Singapore"));
                payments = await _paymentRepository.GetPaymentsByHome(home.Id, from, to);
            }

            return Ok(_mapper.Map<IList<PaymentSimple>>(payments));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPayment(Guid id)
        {
            var payment = _mapper.Map<PaymentDetail>(await _paymentRepository.GetPayment(id));

            return Ok(payment);
        }

        [HttpPost]
        public async Task<IActionResult> CreatePayment(PaymentCreate paymentCreate)
        {
            var home = await _homeRepository.GetHome(paymentCreate.HomeId);
            if (home == null)
                return BadRequest();

            var resident = await _residentRepository.GetResident(paymentCreate.ResidentId);
            if (resident == null)
                return BadRequest();

            var payment = _mapper.Map<Payment>(paymentCreate);
            payment.Home = home;
            payment.Resident = resident;
            _paymentRepository.CreatePayment(payment);
            await _unitOfWork.SaveChanges();

            return CreatedAtAction(nameof(GetPayment), new { Id = payment.Id }, _mapper.Map<PaymentDetail>(payment));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePayment(Guid id, PaymentUpdate paymentUpdate)
        {
            if (id != paymentUpdate.Id)
                return BadRequest();

            var payment = await _paymentRepository.GetPayment(paymentUpdate.Id);
            if (payment == null)
                return NotFound();

            payment.Name = paymentUpdate.Name;
            payment.DateTime = paymentUpdate.DateTime;
            payment.Amount = paymentUpdate.Amount;
            payment.Remarks = paymentUpdate.Remarks;
            payment.CategoryId = paymentUpdate.CategoryId;
            payment.ResidentId = paymentUpdate.ResidentId;

            _paymentRepository.UpdatePayment(payment);
            await _unitOfWork.SaveChanges();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePayment(Guid id)
        {
            var payment = await _paymentRepository.GetPayment(id);

            if (payment == null)
                return NotFound();

            _paymentRepository.DeletePayment(payment);

            await _unitOfWork.SaveChanges();

            return NoContent();
        }

        [HttpGet("stats")]
        public async Task<IActionResult> GetPaymentStatistics(Guid homeId, int? year, int? month)
        {
            if (homeId == null)
                return BadRequest();

            var home = await _homeRepository.GetHome(homeId);
            if (home == null)
                return NotFound();

            var payments = (await _paymentRepository.GetPaymentsByHome(homeId))
                .OrderBy(p => p.DateTime);
            if (payments.Count() == 0)
                return NoContent();

            var paymentStats = new PaymentStats();
            paymentStats.Residents = _mapper.Map<IList<ResidentSimple>>(home.Residents);

            if (year == null || month == null)
            {
                year = DateTime.UtcNow.GetDateTimeInTimeZone("Asia/Singapore").Year;
                month = DateTime.UtcNow.GetDateTimeInTimeZone("Asia/Singapore").Month;
            }

            home.PaymentCategories = home.PaymentCategories
                .OrderBy(pc => pc.Order)
                .ToList();

            paymentStats.CategorisedBreakdown = new List<PaymentStatsCategorisedAmount>();
            foreach (var category in home.PaymentCategories)
                paymentStats.CategorisedBreakdown.Add(new PaymentStatsCategorisedAmount(
                    category.Name,
                    category.Budget,
                    payments
                        .Where(p => p.Category?.Id == category.Id)
                        .Where(p => p.DateTime.GetDateTimeInTimeZone("Asia/Singapore").Year == year)
                        .Where(p => p.DateTime.GetDateTimeInTimeZone("Asia/Singapore").Month == month)
                    )
                );

            var firstPayment = payments.First();
            paymentStats.CategorisedTrend = new List<PaymentStatsCategorisedTrend>();
            foreach (var category in home.PaymentCategories)
                paymentStats.CategorisedTrend.Add(new PaymentStatsCategorisedTrend(category.Name, payments.Where(p => p.Category?.Id == category.Id), firstPayment.DateTime.Year, firstPayment.DateTime.Month));

            return Ok(paymentStats);
        }
    }
}