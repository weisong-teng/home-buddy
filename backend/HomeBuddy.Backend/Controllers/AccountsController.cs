using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HomeBuddy.Backend.Models;
using HomeBuddy.Backend.Models.DTOs;
using HomeBuddy.Backend.Options;
using HomeBuddy.Backend.Persistence;
using HomeBuddy.Backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;

namespace HomeBuddy.Backend.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IResidentRepository _residentRepository;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailService;
        private readonly IntegrationOptions _integrationOptions;

        public AccountsController(
            IAccountService accountService,
            IResidentRepository residentRepository,
            IMapper mapper,
            IOptions<IntegrationOptions> integrationOptionsAccessor,
            IEmailService emailService
        )
        {
            _accountService = accountService;
            _residentRepository = residentRepository;
            _mapper = mapper;
            _integrationOptions = integrationOptionsAccessor.Value;
            _emailService = emailService;
        }

        [HttpGet("me")]
        public async Task<IActionResult> GetInfo()
        {
            var userName = HttpContext.User.Identity?.Name;
            if (userName == null)
                return NotFound();

            var account = await _accountService.GetAccount(userName);
            if (account == null)
                return NotFound();

            var accountDetail = _mapper.Map<AccountDetail>(account);
            accountDetail.Role = (await _accountService.GetRoles(account)).FirstOrDefault();
            accountDetail.Residents = _mapper.Map<IList<ResidentDetail>>(await _residentRepository.GetResidentsByAccount(account));

            return Ok(accountDetail);
        }

        [HttpGet("users")]
        public async Task<IActionResult> GetUsers()
        {
            var accountId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var users = (await _accountService.GetAccounts("User"))
                .Where(a => a.Id != accountId)
                .ToList();

            return Ok(_mapper.Map<IList<AccountSimple>>(users));
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register(AccountRegisterRequest accountRegisterRequest)
        {
            if (await _accountService.GetAccount(accountRegisterRequest.UserName ?? "") != null)
                return BadRequest("Username has already been used");

            var account = _mapper.Map<Account>(accountRegisterRequest);

            var result = await _accountService.CreateAccount(account, accountRegisterRequest.Password ?? "", accountRegisterRequest.IsAdmin);
            var response = new AccountRegisterResponse(result);
            if (response.Succeeded)
                response.AccessToken = await _accountService.Authenticate(account, accountRegisterRequest.Password ?? "");

            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login(AccountLoginRequest request)
        {
            var account = await _accountService.GetAccount(request.UserName ?? "");
            if (account == null)
                return Unauthorized();

            var token = await _accountService.Authenticate(account, request.Password ?? "");

            if (token == null)
                return Unauthorized();
            var response = new AccountLoginResponse();
            response.AccessToken = token;
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost("getResetPasswordToken")]
        public async Task<IActionResult> GetResetPasswordToken([FromQuery] string email)
        {
            var user = await _accountService.GetAccountByEmail(email);

            if (user == null)
                return NotFound();

            var token = await _accountService.GeneratePasswordResetToken(user);
            token = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(token));
            var callbackUrl = $"{ _integrationOptions.WebApp}/resetPassword?accountId={user.Id}&token={token}";

            var emailRequest = new EmailRequest();
            emailRequest.Recipients = new List<string>() { user.Email };
            emailRequest.EmailPurpose = EmailPurpose.ResetPassword;
            emailRequest.CallbackUrl = callbackUrl;
            var isEmailSendSucceed = await _emailService.SendEmail(emailRequest);

            return isEmailSendSucceed ? Ok() : BadRequest();
        }

        [AllowAnonymous]
        [HttpPost("resetPassword")]
        public async Task<IActionResult> ResetPassword(AccountResetPasswordRequest request)
        {
            if (
                string.IsNullOrWhiteSpace(request.AccountId) ||
                string.IsNullOrWhiteSpace(request.Token) ||
                string.IsNullOrWhiteSpace(request.Password)
            )
                return BadRequest();

            var user = await _accountService.GetAccountById(request.AccountId);
            if (user == null)
                return NotFound();

            var token = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(request.Token));
            var result = await _accountService.ResetPassword(user, token, request.Password);

            return result.Succeeded ? Ok() : BadRequest(result.Errors);
        }
    }
}