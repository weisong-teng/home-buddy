using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HomeBuddy.Backend.Models;
using HomeBuddy.Backend.Models.DTOs;
using HomeBuddy.Backend.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace residentBuddy.Backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ResidentsController : ControllerBase
    {
        private readonly IResidentRepository _residentRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ResidentsController(IResidentRepository residentRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _residentRepository = residentRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetResidents()
        {
            var residents = _mapper.Map<IList<ResidentSimple>>(await _residentRepository.GetResidents());

            return Ok(residents);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetResident(Guid id)
        {
            var resident = _mapper.Map<ResidentDetail>(await _residentRepository.GetResident(id));

            return Ok(resident);
        }

        [HttpPost]
        public async Task<IActionResult> CreateResident(ResidentCreate residentCreate)
        {
            var resident = _mapper.Map<Resident>(residentCreate);
            _residentRepository.CreateResident(_mapper.Map<Resident>(resident));
            await _unitOfWork.SaveChanges();

            return CreatedAtAction(nameof(GetResident), new { Id = resident.Id }, resident);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateResident(Guid id, ResidentUpdate residentUpdate)
        {
            if (id != residentUpdate.Id)
                return BadRequest();

            var resident = _mapper.Map<Resident>(residentUpdate);
            _residentRepository.UpdateResident(resident);
            await _unitOfWork.SaveChanges();

            _residentRepository.UpdateResident(resident);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteResident(Guid id)
        {
            var resident = await _residentRepository.GetResident(id);

            if (resident == null)
                return NotFound();

            _residentRepository.DeleteResident(resident);

            await _unitOfWork.SaveChanges();

            return NoContent();
        }
    }
}