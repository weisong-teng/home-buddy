using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeBuddy.Backend.Models;

namespace HomeBuddy.Backend.Extensions
{
    public static class Common
    {
        public static IList<string> GetOwnerAccountId(this Home home)
        {
            return home.Residents
                .Where(r => r.IsOwner)
                .Select(r => r.AccountId)
                .ToList();
        }

        public static DateTime GetDateTimeInTimeZone(this DateTime dateTime, string timeZoneId = "")
        {
            if (string.IsNullOrWhiteSpace(timeZoneId))
                return dateTime;

            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dateTime, timeZoneId);
        }
    }
}