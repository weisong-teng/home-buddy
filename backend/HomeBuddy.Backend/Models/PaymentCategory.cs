using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models
{
    public class PaymentCategory
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public decimal Budget { get; set; }
        public Home Home { get; set; }
        public Guid HomeId { get; set; }
        public ICollection<Payment> Payments { get; set; }

        public PaymentCategory()
        {
            Name = "";
            Payments = new List<Payment>();
        }
    }
}