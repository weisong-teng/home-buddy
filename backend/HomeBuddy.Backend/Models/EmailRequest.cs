using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models
{
    public class EmailRequest
    {
        public IEnumerable<string> Recipients { get; set; }
        public EmailPurpose EmailPurpose { get; set; }
        public string? CallbackUrl { get; set; }

        public EmailRequest()
        {
            Recipients = new Collection<string>();
        }
    }

    public enum EmailPurpose
    {
        ResetPassword
    }
}