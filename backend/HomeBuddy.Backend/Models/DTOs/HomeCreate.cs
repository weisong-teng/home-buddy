using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class HomeCreate
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public byte[]? Picture { get; set; }

        public HomeCreate()
        {
            Name = "";
            Address = "";
            PostalCode = "";
        }
    }
}