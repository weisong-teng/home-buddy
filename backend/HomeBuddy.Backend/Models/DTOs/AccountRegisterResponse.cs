using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class AccountRegisterResponse
    {
        public bool Succeeded { get; set; }
        public IEnumerable<Microsoft.AspNetCore.Identity.IdentityError> Errors { get; set; }
        public string? AccessToken { get; set; }

        public AccountRegisterResponse(IdentityResult accountCreationResult)
        {
            Succeeded = accountCreationResult.Succeeded;
            Errors = accountCreationResult.Errors;
        }
    }
}