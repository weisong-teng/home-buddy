using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class ResidentDetail
    {
        public Guid Id { get; set; }
        public bool IsOwner { get; set; }
        public HomeSimple? Home { get; set; }
    }
}