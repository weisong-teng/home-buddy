using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class PaymentUpdate
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime DateTime { get; set; }
        public decimal Amount { get; set; }
        public string? Remarks { get; set; }
        public Guid? CategoryId { get; set; }
        public Guid ResidentId { get; set; }

        public PaymentUpdate()
        {
            Name = "";
        }
    }
}