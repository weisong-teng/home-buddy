using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class ResidentSimple
    {
        public Guid Id { get; set; }
        public bool IsOwner { get; set; }
        public string? Username { get; set; }
        public string? AccountId { get; set; }
    }
}