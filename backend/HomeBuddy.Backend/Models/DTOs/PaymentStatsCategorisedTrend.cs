using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeBuddy.Backend.Extensions;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class PaymentStatsCategorisedTrend
    {
        public string Category { get; set; }
        public IList<PaymentStatsTrendAmount> TrendItems { get; set; }
        private readonly int _startingYear;
        private readonly int _startingMonth;

        public PaymentStatsCategorisedTrend(string? category, IEnumerable<Payment> payments, int startingYear, int startingMonth)
        {
            Category = string.IsNullOrWhiteSpace(category) ? "NA" : category;
            _startingYear = startingYear;
            _startingMonth = startingMonth;
            TrendItems = GetPaymentStatsTrendAmount();
            foreach (var trendAmount in TrendItems)
                trendAmount.Amount = payments
                    .Where(p => p.DateTime.GetDateTimeInTimeZone("Asia/Singapore").Year == trendAmount.Year && p.DateTime.GetDateTimeInTimeZone("Asia/Singapore").Month == trendAmount.Month)
                    .Sum(p => p.Amount);
        }

        private List<PaymentStatsTrendAmount> GetPaymentStatsTrendAmount()
        {
            var results = new List<PaymentStatsTrendAmount>();
            var currentDate = DateTime.UtcNow;
            for (int year = _startingYear; year <= currentDate.Year; year++)
            {
                for (int month = 1; month <= 12; month++)
                {
                    if (year == _startingYear && month < _startingMonth)
                        continue;
                    if (year == currentDate.Year && month > currentDate.Month)
                        break;
                    results.Add(new PaymentStatsTrendAmount(year, month));
                }
            }

            return results;
        }
    }
}