using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class ResidentCreate
    {
        public string? Name { get; set; }
    }
}