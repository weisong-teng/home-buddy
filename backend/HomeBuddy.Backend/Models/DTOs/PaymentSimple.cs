using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class PaymentSimple
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public DateTime DateTime { get; set; }
        public decimal Amount { get; set; }
        public PaymentCategorySimple? Category { get; set; }
        public ResidentSimple? Resident { get; set; }
    }
}