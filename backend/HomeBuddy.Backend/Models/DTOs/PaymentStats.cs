using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class PaymentStats
    {
        public IList<ResidentSimple>? Residents { get; set; }
        public IList<PaymentStatsCategorisedAmount>? CategorisedBreakdown { get; set; }
        public IList<PaymentStatsCategorisedTrend>? CategorisedTrend { get; set; }
    }
}