using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class AccountResetPasswordRequest
    {
        public string? AccountId { get; set; }
        public string? Token { get; set; }
        public string? Password { get; set; }
    }
}