using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class ResidentUpdate
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
    }
}