using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class HomeDetailForPaymentSetup
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public IEnumerable<ResidentSimple>? Residents { get; set; }
        public IEnumerable<PaymentCategorySimple>? PaymentCategories { get; set; }
    }
}