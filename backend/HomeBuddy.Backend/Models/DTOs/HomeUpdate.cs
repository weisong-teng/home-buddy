using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class HomeUpdate
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public byte[]? Picture { get; set; }
        public IEnumerable<string> AccountsToAdd { get; set; }
        public IEnumerable<Guid> ResidentsToRemove { get; set; }
        public IEnumerable<Guid> ResidentsToUpgrade { get; set; }
        public IEnumerable<Guid> ResidentsToDowngrade { get; set; }
        public IEnumerable<PaymentCategoryCreate> PaymentCategoriesToCreate { get; set; }
        public IEnumerable<PaymentCategoryUpdate> PaymentCategoriesToUpdate { get; set; }
        public IEnumerable<Guid> PaymentCategoriesToDelete { get; set; }

        public HomeUpdate()
        {
            Name = "";
            Address = "";
            PostalCode = "";
            AccountsToAdd = new List<string>();
            ResidentsToRemove = new List<Guid>();
            ResidentsToUpgrade = new List<Guid>();
            ResidentsToDowngrade = new List<Guid>();
            PaymentCategoriesToCreate = new List<PaymentCategoryCreate>();
            PaymentCategoriesToUpdate = new List<PaymentCategoryUpdate>();
            PaymentCategoriesToDelete = new List<Guid>();
        }
    }
}