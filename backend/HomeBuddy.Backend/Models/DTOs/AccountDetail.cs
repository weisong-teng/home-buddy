using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class AccountDetail
    {
        public string Id { get; set; } = "";
        public string? Username { get; set; }
        public string? Email { get; set; }
        public string? Role { get; set; }
        public IList<ResidentDetail>? Residents { get; set; }
    }
}