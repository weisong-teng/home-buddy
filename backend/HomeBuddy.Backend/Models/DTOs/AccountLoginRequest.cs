using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class AccountLoginRequest
    {
        public string? UserName { get; set; }
        public string? Password { get; set; }
    }
}