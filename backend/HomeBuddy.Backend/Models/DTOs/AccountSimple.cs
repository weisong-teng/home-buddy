using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class AccountSimple
    {
        public string Id { get; set; } = "";
        public string? Username { get; set; }
    }
}