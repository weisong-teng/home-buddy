using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class PaymentStatsCategorisedAmount
    {
        public string Category { get; set; }
        public decimal Amount { get; set; }
        public decimal Budget { get; set; }

        public PaymentStatsCategorisedAmount(string? category, decimal budget, IEnumerable<Payment> payments)
        {
            Category = string.IsNullOrWhiteSpace(category) ? "NA" : category;
            Budget = budget;
            Amount = payments.Sum(p => p.Amount);
        }
    }
}