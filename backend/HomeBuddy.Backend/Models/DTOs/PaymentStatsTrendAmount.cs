using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models.DTOs
{
    public class PaymentStatsTrendAmount
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public decimal Amount { get; set; }

        public PaymentStatsTrendAmount(int year, int month)
        {
            Year = year;
            Month = month;
        }
    }
}