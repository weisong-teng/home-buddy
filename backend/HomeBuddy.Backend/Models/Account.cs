using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace HomeBuddy.Backend.Models
{
    public class Account : IdentityUser
    {
        public ICollection<Resident>? Residents { get; set; }
    }
}