using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models
{
    public class Home
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public byte[]? Picture { get; set; }
        public ICollection<Resident> Residents { get; set; }
        public ICollection<Payment> Payments { get; set; }
        public ICollection<PaymentCategory> PaymentCategories { get; set; }

        public Home()
        {
            Name = "";
            Address = "";
            PostalCode = "";
            Residents = new List<Resident>();
            Payments = new List<Payment>();
            PaymentCategories = new List<PaymentCategory>();
        }
    }
}