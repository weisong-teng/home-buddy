using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models
{
    public class Payment
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime DateTime { get; set; }
        public decimal Amount { get; set; }
        public string? Remarks { get; set; }
        public PaymentCategory? Category { get; set; }
        public Guid? CategoryId { get; set; }
        public Home Home { get; set; }
        public Guid HomeId { get; set; }
        public Resident Resident { get; set; }
        public Guid ResidentId { get; set; }

        public Payment()
        {
            Name = "";
        }
    }
}