using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Models
{
    public class Resident
    {
        public Guid Id { get; set; }
        public bool IsOwner { get; set; }
        public Home Home { get; set; }
        public Guid HomeId { get; set; }
        public Account Account { get; set; }
        public string AccountId { get; set; }
        public ICollection<Payment> Payments { get; set; }

        public Resident()
        {

        }

        public Resident(Account account, Home home)
        {
            Account = account;
            AccountId = account.Id;
            Home = home;
            HomeId = home.Id;
        }
    }
}