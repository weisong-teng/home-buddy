﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class SeedIdentityData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "2c5e174e-3b0e-446f-86af-483d56fd7210", "39efa173-c105-4db5-9bdd-1832f6d1c797", "Admin", "ADMIN" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "8e445865-a24d-4543-a6c6-9443d048cdb9", 0, "04c522cb-2478-4b9a-a5a9-4f1fecd16aba", "weisong.teng.work+admin1@gmail.com", false, false, null, "WEISONG.TENG.WORK+ADMIN1@GMAIL.COM", "ADMIN1", "AQAAAAEAACcQAAAAEGtcMcP/hgjz/UgmorbbwTnTSP/D+npq8hvoZDR5IjXpsk45wyzOIy3TOIMsI4JhHg==", null, false, "8d641b19-81f2-43dc-b6cf-81c606707551", false, "admin1" },
                    { "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3", 0, "602ddf75-74e9-4284-b33d-e1db23c88b7a", "weisong.teng.work+user1@gmail.com", false, false, null, "WEISONG.TENG.WORK+USER1@GMAIL.COM", "USER1", "AQAAAAEAACcQAAAAEMMVur48PISWGy8u8mR+xgD8F9eqs9hsd/MjkGn2YWEDtAqTuNTReRQkyyU1F9co8Q==", null, false, "10eb44ce-52e8-433c-b6b8-b40c90022ccd", false, "user1" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "2c5e174e-3b0e-446f-86af-483d56fd7210", "8e445865-a24d-4543-a6c6-9443d048cdb9" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "2c5e174e-3b0e-446f-86af-483d56fd7210", "8e445865-a24d-4543-a6c6-9443d048cdb9" });

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9");
        }
    }
}
