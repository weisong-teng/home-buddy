﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class AddPaymentCategoriesToHome : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "HomeId",
                table: "PaymentCategories",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "53af99ab-6aac-4b87-a4f7-b9e3e797ee1e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "72953f5a-f149-4ef4-bb9e-cfc09975e868",
                column: "ConcurrencyStamp",
                value: "c7fd5cb6-9d46-4ba4-8bd8-81c9ae578174");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "d1c3067a-d3e3-483a-a3f1-8d13c0feeb2a", "AQAAAAEAACcQAAAAEE1EqDUl4ys3SwyT+FrqchMugCOqp4tL32KVa9EA/ugT/4WrEbBWWTjfBCs5KmP2Rg==", "196c6b45-5a25-4c23-8232-abd98a959b99" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2ed8845b-88f6-49df-9e91-97942d5c0c01", "AQAAAAEAACcQAAAAEPhx5haREux7Mj1BkGfqBTk+b46BPq7b9x8nW8szwwd+JE2ckX61bODjsu7quglY8Q==", "eb118df6-6940-4ae6-97dd-befb01f1ea2a" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "dea8e4b6-9c4e-4198-80a5-0f12873e0f90", "AQAAAAEAACcQAAAAEEmfX55NWGUfuulPmzeoXd4vSXz/U45vZ9R636DFFv0ZP7w4RWvizlQUBypSsZv2SQ==", "f8f4ab9a-3753-4b1d-bd21-96e6514d12f2" });

            migrationBuilder.UpdateData(
                table: "PaymentCategories",
                keyColumn: "Id",
                keyValue: new Guid("16ca6564-a12e-4858-b78a-8dd0ceec2266"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.UpdateData(
                table: "PaymentCategories",
                keyColumn: "Id",
                keyValue: new Guid("3948af5c-1d5c-4eca-8ef1-7b4502490dc8"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.UpdateData(
                table: "PaymentCategories",
                keyColumn: "Id",
                keyValue: new Guid("39497e24-20ff-4990-811e-aaf494084b9f"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.UpdateData(
                table: "PaymentCategories",
                keyColumn: "Id",
                keyValue: new Guid("5902c45d-0dc4-488a-8bff-2457c3868014"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.UpdateData(
                table: "PaymentCategories",
                keyColumn: "Id",
                keyValue: new Guid("6a0ba162-0767-4fd8-b1f5-d4f9f780f6ca"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.UpdateData(
                table: "PaymentCategories",
                keyColumn: "Id",
                keyValue: new Guid("fd1ef8e1-9864-4214-a9e2-9a7f1ec86962"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.CreateIndex(
                name: "IX_PaymentCategories_HomeId",
                table: "PaymentCategories",
                column: "HomeId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentCategories_Homes_HomeId",
                table: "PaymentCategories",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaymentCategories_Homes_HomeId",
                table: "PaymentCategories");

            migrationBuilder.DropIndex(
                name: "IX_PaymentCategories_HomeId",
                table: "PaymentCategories");

            migrationBuilder.DropColumn(
                name: "HomeId",
                table: "PaymentCategories");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "4e8f878f-3e1b-4c90-a9db-147ff952171e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "72953f5a-f149-4ef4-bb9e-cfc09975e868",
                column: "ConcurrencyStamp",
                value: "2fcccb88-1693-41f7-9126-4fa1c86d9a43");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c5b3de79-3a66-4d26-802e-3ea59606645d", "AQAAAAEAACcQAAAAEEcvhRWQYlDToMeytgXAMp7BNEsgpMAqV04y3Uc0oJDe53WCP7IESBmzqSJY6c1ASA==", "3c6eb977-9f50-4884-94d5-bef66347ad1d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "a15ec4c1-52aa-42e1-ac99-8dca50c4e6c7", "AQAAAAEAACcQAAAAEN3b9mCHxTFSs6v3l5rY6eXg1z/Aer/700kxfznP9GOpLpqX8PX6z58YhpgzhpXVQA==", "ef79dc07-0ddf-45c5-a455-2b8f609d4e2d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "d9aa76bc-70fe-421c-a2bd-f2be421b0193", "AQAAAAEAACcQAAAAEGoxIpv0wtg+Pm+rSMclwAqFu6hHvVUGZlas0t0bl5YWZdeB6LjiJ2fsVDYFRLdoMg==", "c6578725-6b59-4573-83f2-56e726f35f93" });
        }
    }
}
