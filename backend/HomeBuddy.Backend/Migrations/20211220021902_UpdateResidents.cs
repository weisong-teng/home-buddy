﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class UpdateResidents : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Residents_Homes_HomeId",
                table: "Residents");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Residents");

            migrationBuilder.AlterColumn<Guid>(
                name: "HomeId",
                table: "Residents",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "e97aab12-47b7-4a04-8011-290a5f0fb383");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "75a978bf-1d7b-484b-ac88-1366db4cadaf", "AQAAAAEAACcQAAAAEBQMCt/duaci1HWhArA9sQNvspaZX2mzeJbLUNSJ1q3epSPqT0w8kOv7A4oxm/6rbw==", "9bc54bd4-b083-4107-a2fa-7d580c4dc8c2" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "02a37f43-aa93-4930-a6aa-21ae712eb568", "AQAAAAEAACcQAAAAEKO7ms3hWeY6v3c+ci4eKqM/4pc/3rI20rhFqhktJr6gP4j2T6Cu7Iqck753xjaC1Q==", "7d537624-c421-485c-bcc4-bcf610f387c9" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0d895631-6edb-4473-9657-e972b3e906ed", "AQAAAAEAACcQAAAAEADb+9f3s0tQiZaFUijdMLI6urJYrJjEYY9bntODfVKdmk9WS1WMrnbkFY9qgfNYog==", "ed8791bf-495f-47a0-b5c5-08b5e6581243" });

            migrationBuilder.AddForeignKey(
                name: "FK_Residents_Homes_HomeId",
                table: "Residents",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Residents_Homes_HomeId",
                table: "Residents");

            migrationBuilder.AlterColumn<Guid>(
                name: "HomeId",
                table: "Residents",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Residents",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "aac763e7-649e-4f59-8a5a-4b9d5d1bf5aa");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b29f7dfb-c7e4-4db8-8fa6-ce9edacbdce8", "AQAAAAEAACcQAAAAENB4CXIImKOdK2rYWdiO2OanBxgb0/DpNoC/1X5SHlwibxUAeJ3Dcd9CKC3XT4Zqrw==", "3f5d21cf-af57-4418-ae1f-6421f4d70c14" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "bb93f487-7f3d-48ae-ad7b-cfe9ba35ec7f", "AQAAAAEAACcQAAAAEGETAPQ8GlTsn4CoMTLIbPO5IgLFKFglH8Gkvs3balfryiAGQpgOOdd3J6SakHHRyA==", "394b1f9c-b83f-48b2-b216-60dd1e6390c7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b97a592b-ce64-419e-add4-8ce1e020bf77", "AQAAAAEAACcQAAAAELmSLtlivM42/ZWRGevoM3mQSM6tyEFq4TFW/sR48r9st8rFEKjGlDhE68y44WM1ag==", "77853600-9fb7-43af-bc9d-d4df78edf8f1" });

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("1790bfca-812a-40ba-ac29-fe8b3e1b6836"),
                column: "Name",
                value: "Resident 3");

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"),
                column: "Name",
                value: "Resident 2");

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("66ce2822-f720-485b-a56f-7e28b609503a"),
                column: "Name",
                value: "Resident 4");

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("884d167d-b30c-49fb-b30e-254bece11bf5"),
                column: "Name",
                value: "Resident 1");

            migrationBuilder.AddForeignKey(
                name: "FK_Residents_Homes_HomeId",
                table: "Residents",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
