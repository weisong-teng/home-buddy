﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class AddAddressInfoToHomes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Homes",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PostalCode",
                table: "Homes",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "Homes",
                keyColumn: "Id",
                keyValue: new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"),
                columns: new[] { "Address", "PostalCode" },
                values: new object[] { "No 2 Street 12", "008605" });

            migrationBuilder.UpdateData(
                table: "Homes",
                keyColumn: "Id",
                keyValue: new Guid("32a58340-a987-421b-9f66-a6db89f55777"),
                columns: new[] { "Address", "PostalCode" },
                values: new object[] { "Block 1 Street 1 #01-23", "123456" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Homes");

            migrationBuilder.DropColumn(
                name: "PostalCode",
                table: "Homes");
        }
    }
}
