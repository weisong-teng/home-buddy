﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class MakeHomeNotNullableInResidents : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Residents_Homes_HomeId",
                table: "Residents");

            migrationBuilder.AlterColumn<Guid>(
                name: "HomeId",
                table: "Residents",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Residents_Homes_HomeId",
                table: "Residents",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Residents_Homes_HomeId",
                table: "Residents");

            migrationBuilder.AlterColumn<Guid>(
                name: "HomeId",
                table: "Residents",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddForeignKey(
                name: "FK_Residents_Homes_HomeId",
                table: "Residents",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id");
        }
    }
}
