﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class AddPaymentCategoryAmountRemarksToPayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Amount",
                table: "Payments",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<Guid>(
                name: "CategoryId",
                table: "Payments",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Remarks",
                table: "Payments",
                type: "text",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PaymentCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentCategories", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "PaymentCategories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("16ca6564-a12e-4858-b78a-8dd0ceec2266"), "Grocery" },
                    { new Guid("3948af5c-1d5c-4eca-8ef1-7b4502490dc8"), "Broadband" },
                    { new Guid("39497e24-20ff-4990-811e-aaf494084b9f"), "Rental" },
                    { new Guid("5902c45d-0dc4-488a-8bff-2457c3868014"), "Furniture" },
                    { new Guid("6a0ba162-0767-4fd8-b1f5-d4f9f780f6ca"), "Other" },
                    { new Guid("fd1ef8e1-9864-4214-a9e2-9a7f1ec86962"), "Utilities" }
                });

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "Id",
                keyValue: new Guid("108e7c96-ea21-44f2-9cbe-db4237c2d1dd"),
                columns: new[] { "Amount", "CategoryId" },
                values: new object[] { 10m, new Guid("39497e24-20ff-4990-811e-aaf494084b9f") });

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "Id",
                keyValue: new Guid("41881c20-df28-43df-8e1c-e42748181ea3"),
                columns: new[] { "Amount", "CategoryId" },
                values: new object[] { 21.03m, new Guid("16ca6564-a12e-4858-b78a-8dd0ceec2266") });

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "Id",
                keyValue: new Guid("8e445865-a24d-4543-a6c6-9443d048cdb9"),
                columns: new[] { "Amount", "CategoryId" },
                values: new object[] { 20m, new Guid("16ca6564-a12e-4858-b78a-8dd0ceec2266") });

            migrationBuilder.CreateIndex(
                name: "IX_Payments_CategoryId",
                table: "Payments",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_PaymentCategories_CategoryId",
                table: "Payments",
                column: "CategoryId",
                principalTable: "PaymentCategories",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_PaymentCategories_CategoryId",
                table: "Payments");

            migrationBuilder.DropTable(
                name: "PaymentCategories");

            migrationBuilder.DropIndex(
                name: "IX_Payments_CategoryId",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "Remarks",
                table: "Payments");
        }
    }
}
