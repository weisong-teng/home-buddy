﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class ChangeHomeAndResidentsToOneToMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HomeResident");

            migrationBuilder.AddColumn<Guid>(
                name: "HomeId",
                table: "Residents",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "47b56264-2bda-455d-ae09-7143acc795e7");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "81fee4f6-4151-4946-be00-4ecf6b340aca", "AQAAAAEAACcQAAAAEPFvreRGpi83wOW3TeqywDua1Lk0PMO2xYUqswxLlNxEgqcQ6bF54cCOfNpYr+LfPA==", "5817fec7-cb00-4bed-b629-6fc3dd5f2bc2" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "67ce894f-caad-4697-870d-1613d98e1994", "AQAAAAEAACcQAAAAEPj96fSqsb9Jr7s3M8HYzSR/92WO0Sfrftj52LdKHbINWcUkFxNI6sbGJ3jDC6SF3g==", "afe221a6-0324-4191-a91a-cd0f9534ebcb" });

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("1790bfca-812a-40ba-ac29-fe8b3e1b6836"),
                column: "HomeId",
                value: new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"));

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("66ce2822-f720-485b-a56f-7e28b609503a"),
                column: "HomeId",
                value: new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"));

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("884d167d-b30c-49fb-b30e-254bece11bf5"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.CreateIndex(
                name: "IX_Residents_HomeId",
                table: "Residents",
                column: "HomeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Residents_Homes_HomeId",
                table: "Residents",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Residents_Homes_HomeId",
                table: "Residents");

            migrationBuilder.DropIndex(
                name: "IX_Residents_HomeId",
                table: "Residents");

            migrationBuilder.DropColumn(
                name: "HomeId",
                table: "Residents");

            migrationBuilder.CreateTable(
                name: "HomeResident",
                columns: table => new
                {
                    HomeId = table.Column<Guid>(type: "uuid", nullable: false),
                    ResidentId = table.Column<Guid>(type: "uuid", nullable: false),
                    JoinAt = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HomeResident", x => new { x.HomeId, x.ResidentId });
                    table.ForeignKey(
                        name: "FK_HomeResident_Homes_HomeId",
                        column: x => x.HomeId,
                        principalTable: "Homes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HomeResident_Residents_ResidentId",
                        column: x => x.ResidentId,
                        principalTable: "Residents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "39efa173-c105-4db5-9bdd-1832f6d1c797");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "04c522cb-2478-4b9a-a5a9-4f1fecd16aba", "AQAAAAEAACcQAAAAEGtcMcP/hgjz/UgmorbbwTnTSP/D+npq8hvoZDR5IjXpsk45wyzOIy3TOIMsI4JhHg==", "8d641b19-81f2-43dc-b6cf-81c606707551" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "602ddf75-74e9-4284-b33d-e1db23c88b7a", "AQAAAAEAACcQAAAAEMMVur48PISWGy8u8mR+xgD8F9eqs9hsd/MjkGn2YWEDtAqTuNTReRQkyyU1F9co8Q==", "10eb44ce-52e8-433c-b6b8-b40c90022ccd" });

            migrationBuilder.InsertData(
                table: "HomeResident",
                columns: new[] { "HomeId", "ResidentId", "JoinAt" },
                values: new object[,]
                {
                    { new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"), new Guid("1790bfca-812a-40ba-ac29-fe8b3e1b6836"), new DateTime(2021, 4, 12, 15, 0, 0, 0, DateTimeKind.Utc) },
                    { new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"), new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"), new DateTime(2021, 4, 12, 15, 0, 0, 0, DateTimeKind.Utc) },
                    { new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"), new Guid("884d167d-b30c-49fb-b30e-254bece11bf5"), new DateTime(2021, 4, 10, 15, 0, 0, 0, DateTimeKind.Utc) },
                    { new Guid("32a58340-a987-421b-9f66-a6db89f55777"), new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"), new DateTime(2021, 4, 6, 15, 0, 0, 0, DateTimeKind.Utc) },
                    { new Guid("32a58340-a987-421b-9f66-a6db89f55777"), new Guid("884d167d-b30c-49fb-b30e-254bece11bf5"), new DateTime(2021, 4, 5, 15, 0, 0, 0, DateTimeKind.Utc) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_HomeResident_ResidentId",
                table: "HomeResident",
                column: "ResidentId");
        }
    }
}
