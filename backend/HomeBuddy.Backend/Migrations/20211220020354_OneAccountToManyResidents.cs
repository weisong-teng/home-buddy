﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class OneAccountToManyResidents : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountId",
                table: "Residents",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "aac763e7-649e-4f59-8a5a-4b9d5d1bf5aa");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b29f7dfb-c7e4-4db8-8fa6-ce9edacbdce8", "AQAAAAEAACcQAAAAENB4CXIImKOdK2rYWdiO2OanBxgb0/DpNoC/1X5SHlwibxUAeJ3Dcd9CKC3XT4Zqrw==", "3f5d21cf-af57-4418-ae1f-6421f4d70c14" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "bb93f487-7f3d-48ae-ad7b-cfe9ba35ec7f", "AQAAAAEAACcQAAAAEGETAPQ8GlTsn4CoMTLIbPO5IgLFKFglH8Gkvs3balfryiAGQpgOOdd3J6SakHHRyA==", "394b1f9c-b83f-48b2-b216-60dd1e6390c7" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "f22bdbe5-bf78-407e-908f-2fbb034e6c3d", 0, "b97a592b-ce64-419e-add4-8ce1e020bf77", "weisong.teng.work+user2@gmail.com", false, false, null, "WEISONG.TENG.WORK+USER2@GMAIL.COM", "USER2", "AQAAAAEAACcQAAAAELmSLtlivM42/ZWRGevoM3mQSM6tyEFq4TFW/sR48r9st8rFEKjGlDhE68y44WM1ag==", null, false, "77853600-9fb7-43af-bc9d-d4df78edf8f1", false, "user2" });

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"),
                column: "AccountId",
                value: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3");

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("884d167d-b30c-49fb-b30e-254bece11bf5"),
                column: "AccountId",
                value: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3");

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("1790bfca-812a-40ba-ac29-fe8b3e1b6836"),
                column: "AccountId",
                value: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d");

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("66ce2822-f720-485b-a56f-7e28b609503a"),
                column: "AccountId",
                value: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d");

            migrationBuilder.CreateIndex(
                name: "IX_Residents_AccountId",
                table: "Residents",
                column: "AccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_Residents_AspNetUsers_AccountId",
                table: "Residents",
                column: "AccountId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Residents_AspNetUsers_AccountId",
                table: "Residents");

            migrationBuilder.DropIndex(
                name: "IX_Residents_AccountId",
                table: "Residents");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d");

            migrationBuilder.DropColumn(
                name: "AccountId",
                table: "Residents");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "47b56264-2bda-455d-ae09-7143acc795e7");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "81fee4f6-4151-4946-be00-4ecf6b340aca", "AQAAAAEAACcQAAAAEPFvreRGpi83wOW3TeqywDua1Lk0PMO2xYUqswxLlNxEgqcQ6bF54cCOfNpYr+LfPA==", "5817fec7-cb00-4bed-b629-6fc3dd5f2bc2" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "67ce894f-caad-4697-870d-1613d98e1994", "AQAAAAEAACcQAAAAEPj96fSqsb9Jr7s3M8HYzSR/92WO0Sfrftj52LdKHbINWcUkFxNI6sbGJ3jDC6SF3g==", "afe221a6-0324-4191-a91a-cd0f9534ebcb" });
        }
    }
}
