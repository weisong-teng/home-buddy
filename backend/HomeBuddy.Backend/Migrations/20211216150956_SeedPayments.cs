﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class SeedPayments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Payments",
                columns: new[] { "Id", "DateTime", "Name" },
                values: new object[,]
                {
                    { new Guid("108e7c96-ea21-44f2-9cbe-db4237c2d1dd"), new DateTime(2021, 1, 5, 15, 0, 0, 0, DateTimeKind.Utc), "Payment 1" },
                    { new Guid("41881c20-df28-43df-8e1c-e42748181ea3"), new DateTime(2021, 7, 9, 3, 0, 0, 0, DateTimeKind.Utc), "Payment 1" },
                    { new Guid("8e445865-a24d-4543-a6c6-9443d048cdb9"), new DateTime(2021, 2, 14, 2, 0, 0, 0, DateTimeKind.Utc), "Payment 2" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Payments",
                keyColumn: "Id",
                keyValue: new Guid("108e7c96-ea21-44f2-9cbe-db4237c2d1dd"));

            migrationBuilder.DeleteData(
                table: "Payments",
                keyColumn: "Id",
                keyValue: new Guid("41881c20-df28-43df-8e1c-e42748181ea3"));

            migrationBuilder.DeleteData(
                table: "Payments",
                keyColumn: "Id",
                keyValue: new Guid("8e445865-a24d-4543-a6c6-9443d048cdb9"));
        }
    }
}
