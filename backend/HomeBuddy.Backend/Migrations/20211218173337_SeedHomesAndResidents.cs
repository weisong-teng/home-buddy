﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class SeedHomesAndResidents : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Homes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"), "Home 2" },
                    { new Guid("32a58340-a987-421b-9f66-a6db89f55777"), "Home 1" }
                });

            migrationBuilder.InsertData(
                table: "Residents",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("1790bfca-812a-40ba-ac29-fe8b3e1b6836"), "Resident 3" },
                    { new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"), "Resident 2" },
                    { new Guid("66ce2822-f720-485b-a56f-7e28b609503a"), "Resident 4" },
                    { new Guid("884d167d-b30c-49fb-b30e-254bece11bf5"), "Resident 1" }
                });

            migrationBuilder.InsertData(
                table: "HomeResident",
                columns: new[] { "HomeId", "ResidentId", "JoinAt" },
                values: new object[,]
                {
                    { new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"), new Guid("1790bfca-812a-40ba-ac29-fe8b3e1b6836"), new DateTime(2021, 4, 12, 15, 0, 0, 0, DateTimeKind.Utc) },
                    { new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"), new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"), new DateTime(2021, 4, 12, 15, 0, 0, 0, DateTimeKind.Utc) },
                    { new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"), new Guid("884d167d-b30c-49fb-b30e-254bece11bf5"), new DateTime(2021, 4, 10, 15, 0, 0, 0, DateTimeKind.Utc) },
                    { new Guid("32a58340-a987-421b-9f66-a6db89f55777"), new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"), new DateTime(2021, 4, 6, 15, 0, 0, 0, DateTimeKind.Utc) },
                    { new Guid("32a58340-a987-421b-9f66-a6db89f55777"), new Guid("884d167d-b30c-49fb-b30e-254bece11bf5"), new DateTime(2021, 4, 5, 15, 0, 0, 0, DateTimeKind.Utc) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "HomeResident",
                keyColumns: new[] { "HomeId", "ResidentId" },
                keyValues: new object[] { new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"), new Guid("1790bfca-812a-40ba-ac29-fe8b3e1b6836") });

            migrationBuilder.DeleteData(
                table: "HomeResident",
                keyColumns: new[] { "HomeId", "ResidentId" },
                keyValues: new object[] { new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"), new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778") });

            migrationBuilder.DeleteData(
                table: "HomeResident",
                keyColumns: new[] { "HomeId", "ResidentId" },
                keyValues: new object[] { new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"), new Guid("884d167d-b30c-49fb-b30e-254bece11bf5") });

            migrationBuilder.DeleteData(
                table: "HomeResident",
                keyColumns: new[] { "HomeId", "ResidentId" },
                keyValues: new object[] { new Guid("32a58340-a987-421b-9f66-a6db89f55777"), new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778") });

            migrationBuilder.DeleteData(
                table: "HomeResident",
                keyColumns: new[] { "HomeId", "ResidentId" },
                keyValues: new object[] { new Guid("32a58340-a987-421b-9f66-a6db89f55777"), new Guid("884d167d-b30c-49fb-b30e-254bece11bf5") });

            migrationBuilder.DeleteData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("66ce2822-f720-485b-a56f-7e28b609503a"));

            migrationBuilder.DeleteData(
                table: "Homes",
                keyColumn: "Id",
                keyValue: new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"));

            migrationBuilder.DeleteData(
                table: "Homes",
                keyColumn: "Id",
                keyValue: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.DeleteData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("1790bfca-812a-40ba-ac29-fe8b3e1b6836"));

            migrationBuilder.DeleteData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"));

            migrationBuilder.DeleteData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("884d167d-b30c-49fb-b30e-254bece11bf5"));
        }
    }
}
