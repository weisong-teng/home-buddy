﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class AddResidentRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("66ce2822-f720-485b-a56f-7e28b609503a"));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "9a52d14e-cba4-4874-9e83-c501fb159a0c");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "72953f5a-f149-4ef4-bb9e-cfc09975e868", "26b3177d-4dc0-4075-8d9d-b63bcd44a085", "Resident", "RESIDENT" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "eda1432f-311f-40c5-a5f8-27f0b3bafc60", "AQAAAAEAACcQAAAAEMuLSstkccJvtKVL+zBBKdyoHBbT37P5k646x8oJYwtq66Vaho9GLj9TzrZRRGaXcw==", "a41d8bff-dd4f-4638-a053-15526cb047ef" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9b6538af-1208-47a8-a313-ad3942249f20", "AQAAAAEAACcQAAAAEKTiHb9HyXwOz6nJYjRpHpUpPWi8fVbTUf7EY6hWvyllCSyTk9IMP65Qe0lrSI9sCg==", "f509ef8d-221d-44dd-aa72-6af8ccbd8c32" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c227c032-7374-4164-bfd2-56e285cd3b7b", "AQAAAAEAACcQAAAAEFPNzx6cnlu/pcTwSwj99QpIX2N/CBaLJI96TsBlpuUPdarYr/JugJDtz/pFbHjflw==", "587064a0-189f-47d6-b23d-032bdbc7c4b9" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "72953f5a-f149-4ef4-bb9e-cfc09975e868", "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3" },
                    { "72953f5a-f149-4ef4-bb9e-cfc09975e868", "f22bdbe5-bf78-407e-908f-2fbb034e6c3d" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "72953f5a-f149-4ef4-bb9e-cfc09975e868", "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "72953f5a-f149-4ef4-bb9e-cfc09975e868", "f22bdbe5-bf78-407e-908f-2fbb034e6c3d" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "72953f5a-f149-4ef4-bb9e-cfc09975e868");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "e97aab12-47b7-4a04-8011-290a5f0fb383");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "75a978bf-1d7b-484b-ac88-1366db4cadaf", "AQAAAAEAACcQAAAAEBQMCt/duaci1HWhArA9sQNvspaZX2mzeJbLUNSJ1q3epSPqT0w8kOv7A4oxm/6rbw==", "9bc54bd4-b083-4107-a2fa-7d580c4dc8c2" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "02a37f43-aa93-4930-a6aa-21ae712eb568", "AQAAAAEAACcQAAAAEKO7ms3hWeY6v3c+ci4eKqM/4pc/3rI20rhFqhktJr6gP4j2T6Cu7Iqck753xjaC1Q==", "7d537624-c421-485c-bcc4-bcf610f387c9" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0d895631-6edb-4473-9657-e972b3e906ed", "AQAAAAEAACcQAAAAEADb+9f3s0tQiZaFUijdMLI6urJYrJjEYY9bntODfVKdmk9WS1WMrnbkFY9qgfNYog==", "ed8791bf-495f-47a0-b5c5-08b5e6581243" });

            migrationBuilder.InsertData(
                table: "Residents",
                columns: new[] { "Id", "AccountId", "HomeId" },
                values: new object[] { new Guid("66ce2822-f720-485b-a56f-7e28b609503a"), "f22bdbe5-bf78-407e-908f-2fbb034e6c3d", new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a") });
        }
    }
}
