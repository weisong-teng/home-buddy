﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class AddHomeToPayments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "HomeId",
                table: "Payments",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "Id",
                keyValue: new Guid("108e7c96-ea21-44f2-9cbe-db4237c2d1dd"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "Id",
                keyValue: new Guid("41881c20-df28-43df-8e1c-e42748181ea3"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.UpdateData(
                table: "Payments",
                keyColumn: "Id",
                keyValue: new Guid("8e445865-a24d-4543-a6c6-9443d048cdb9"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));

            migrationBuilder.CreateIndex(
                name: "IX_Payments_HomeId",
                table: "Payments",
                column: "HomeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Homes_HomeId",
                table: "Payments",
                column: "HomeId",
                principalTable: "Homes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Homes_HomeId",
                table: "Payments");

            migrationBuilder.DropIndex(
                name: "IX_Payments_HomeId",
                table: "Payments");

            migrationBuilder.DropColumn(
                name: "HomeId",
                table: "Payments");
        }
    }
}
