﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class UpdateSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "e2f22288-ea23-4c0b-9c16-3cd452163594");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "72953f5a-f149-4ef4-bb9e-cfc09975e868",
                column: "ConcurrencyStamp",
                value: "42c21248-a2bc-4203-9f13-8c661add1b8a");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "16c527f9-984f-4a53-a11a-bc93d784ae0d", "AQAAAAEAACcQAAAAEMVI4XaJKj2UzLryND3bbY7/78Vuk8vADeILK2Knz+oimZgL4DiSd+hFLCcESPniPA==", "6d1b3422-af6e-44e3-a785-b8dbd178e141" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "6f0ca995-d4b5-44be-b628-f608b0dc8543", "AQAAAAEAACcQAAAAENBbq8oHERJdQc/BRy51B4oU+OTlp6pACeIDyK6V56vsqfrxznl/RgowsaUsG++gwQ==", "039b37e2-5696-4f10-831b-0a2b2c485ed6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3387eb5a-d642-4023-8224-d30f98d498b9", "AQAAAAEAACcQAAAAELztgZj04FSHQv+li5h1ftGmPUeelzuOd/r0p7RmilvgPiabBy8fj2etB74p3NRPkw==", "ffbc24eb-c175-4102-b865-807fea213c7e" });

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"),
                column: "HomeId",
                value: new Guid("256d55c3-aadc-4f8c-ba47-20971c75060a"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "9a52d14e-cba4-4874-9e83-c501fb159a0c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "72953f5a-f149-4ef4-bb9e-cfc09975e868",
                column: "ConcurrencyStamp",
                value: "26b3177d-4dc0-4075-8d9d-b63bcd44a085");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "eda1432f-311f-40c5-a5f8-27f0b3bafc60", "AQAAAAEAACcQAAAAEMuLSstkccJvtKVL+zBBKdyoHBbT37P5k646x8oJYwtq66Vaho9GLj9TzrZRRGaXcw==", "a41d8bff-dd4f-4638-a053-15526cb047ef" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9b6538af-1208-47a8-a313-ad3942249f20", "AQAAAAEAACcQAAAAEKTiHb9HyXwOz6nJYjRpHpUpPWi8fVbTUf7EY6hWvyllCSyTk9IMP65Qe0lrSI9sCg==", "f509ef8d-221d-44dd-aa72-6af8ccbd8c32" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c227c032-7374-4164-bfd2-56e285cd3b7b", "AQAAAAEAACcQAAAAEFPNzx6cnlu/pcTwSwj99QpIX2N/CBaLJI96TsBlpuUPdarYr/JugJDtz/pFbHjflw==", "587064a0-189f-47d6-b23d-032bdbc7c4b9" });

            migrationBuilder.UpdateData(
                table: "Residents",
                keyColumn: "Id",
                keyValue: new Guid("656e5f46-bd2c-42c7-a6b9-c0c1f42bd778"),
                column: "HomeId",
                value: new Guid("32a58340-a987-421b-9f66-a6db89f55777"));
        }
    }
}
