﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HomeBuddy.Backend.Migrations
{
    public partial class RenameResidentToUserRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "03e09ad0-36fe-42d6-a680-3e40830ad83c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "72953f5a-f149-4ef4-bb9e-cfc09975e868",
                columns: new[] { "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "028a0597-9fb2-4079-95ff-b2e0910a8ae4", "User", "USER" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "5323d6d5-a826-4812-b918-804f8a9f1b25", "AQAAAAEAACcQAAAAEMDoqDIVYwv7I8mtzQHZEHJ96Xti+yiYET99q+TTqMI+uTQBQdaUym4HjQyk8hiGlA==", "dae25f76-0d7a-427f-a65a-603a593abdf6" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "304a0b8b-d289-4613-83d0-37e788b49c29", "AQAAAAEAACcQAAAAEKJMSZArHgdpgXVKX6EWVaDqvobiHR7CoCTUPSl+d5Wzs7klGfTgDpN4lI0QCjwtMg==", "f7f0ed94-7a44-4f57-a83d-92dc2f4ef2be" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c6e71132-7f7f-4ba8-b70c-42d49795003e", "AQAAAAEAACcQAAAAEFW1+KR0CWhrBVGF4V/RI8CJi+vBf8Uv5RcL2bjRIHpb8NO6UkRRKCmHMsxjDLh66w==", "f7a01935-96ca-4f29-a4eb-9efc7a068285" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2c5e174e-3b0e-446f-86af-483d56fd7210",
                column: "ConcurrencyStamp",
                value: "6dfc27fa-ca0e-408f-b16c-6065763fbce5");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "72953f5a-f149-4ef4-bb9e-cfc09975e868",
                columns: new[] { "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "959df301-5e7a-4b6a-84d0-fdd0d31eef43", "Resident", "RESIDENT" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "8e445865-a24d-4543-a6c6-9443d048cdb9",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "86344d29-f4c8-4292-83ec-e95230cdb7c9", "AQAAAAEAACcQAAAAEAm2geSp4foZPrgdnn6RIrGrnpoWkSFFlpsvuI/D+A6AxZTsv4BNU1fCyQyTg2LrAw==", "28b22304-1d4c-4de7-af8f-4b1c5f484994" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b7c5c951-2113-4c75-a4d0-4b9f1cb593f3",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b72bac3d-b02e-4fb6-93a7-a736b0d7d13a", "AQAAAAEAACcQAAAAENEdp6mGgQEsxFJoddrlFT7v8oPnuT4hbqn4ZNgaOTbry3UXfK0t9fZ+QBNiVohd6Q==", "37e55d82-7ac7-4034-9e14-75876d90a36a" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f22bdbe5-bf78-407e-908f-2fbb034e6c3d",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "bf78d7b3-a6cf-4582-b113-0fe30d5634a0", "AQAAAAEAACcQAAAAEJxRMfrWF0nE2gfl9bp1HDSKdGOVIVwBWB2/R8N1wTibwTxqE/LNQHwOMVYWKGVlBQ==", "049d6fe3-2194-4a3e-afcf-78acdd8e1287" });
        }
    }
}
