using System.Text;
using HomeBuddy.Backend.Mappings;
using HomeBuddy.Backend.Models;
using HomeBuddy.Backend.Options;
using HomeBuddy.Backend.Persistence;
using HomeBuddy.Backend.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<BackendDbContext>(optionsAction => optionsAction.UseNpgsql(builder.Configuration.GetConnectionString("Default")));
builder.Services.AddScoped<IPaymentRepository, PaymentRepository>();
builder.Services.AddScoped<IHomeRepository, HomeRepository>();
builder.Services.AddScoped<IResidentRepository, ResidentRepository>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddIdentity<Account, IdentityRole>()
    .AddEntityFrameworkStores<BackendDbContext>()
    .AddDefaultTokenProviders();

builder.Services.AddScoped<IAccountService, AccountService>();
builder.Services.AddScoped<IEmailService, EmailService>();

builder.Services.AddAutoMapper(configAction => configAction.AddProfile<MappingProfile>());

builder.Services.AddCors(setupAction =>
{
    setupAction.AddPolicy("Default", configurePolicy =>
    {
        configurePolicy
            .WithOrigins(builder.Configuration.GetSection("Security:AllowedOrigins").Get<string[]>())
            .AllowAnyHeader()
            .AllowAnyMethod();
    });
});

builder.Services.Configure<IntegrationOptions>(builder.Configuration.GetSection("Integration"));
builder.Services.Configure<SendGridOptions>(builder.Configuration.GetSection("SendGrid"));

var audience = builder.Configuration.GetSection("Authentication:Audience").Get<string>();
var issuer = builder.Configuration.GetSection("Authentication:Issuer").Get<string>();
var secretKey = builder.Configuration.GetSection("Authentication:SecretKey").Get<string>();
builder.Services
    .AddAuthentication(configureOptions =>
    {
        configureOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        configureOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        configureOptions.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(configureOptions =>
    {
        configureOptions.SaveToken = true;
        configureOptions.RequireHttpsMetadata = true;
        configureOptions.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidAudience = audience,
            ValidIssuer = issuer,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey))
        };
    });

builder.Services.Configure<ForwardedHeadersOptions>(options =>
  {
      options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
      options.ForwardLimit = 2;
      options.KnownNetworks.Clear();
      options.KnownProxies.Clear();
  });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseForwardedHeaders();

app.UseHttpsRedirection();

app.UseCors("Default");

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
