using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HomeBuddy.Backend.Extensions;
using HomeBuddy.Backend.Models;
using HomeBuddy.Backend.Models.DTOs;

namespace HomeBuddy.Backend.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Payment, PaymentDetail>();
            CreateMap<Payment, PaymentSimple>();
            CreateMap<PaymentCreate, Payment>()
                .ForMember(p => p.HomeId, memberOptions => memberOptions.Ignore())
                .ForMember(p => p.ResidentId, memberOptions => memberOptions.Ignore());
            CreateMap<PaymentUpdate, Payment>();

            CreateMap<PaymentCategory, PaymentCategorySimple>();
            CreateMap<PaymentCategoryCreate, PaymentCategory>()
                .ForMember(pc => pc.HomeId, memberOptions => memberOptions.Ignore());

            CreateMap<Home, HomeDetail>();
            CreateMap<Home, HomeDetailForPaymentSetup>();
            CreateMap<Home, HomeSimple>()
                .ForMember(hs => hs.OwnerAccountIds, memberOptions => memberOptions.MapFrom(h => h.GetOwnerAccountId()));
            CreateMap<HomeCreate, Home>();
            CreateMap<HomeUpdate, Home>();

            CreateMap<Resident, ResidentDetail>();
            CreateMap<Resident, ResidentSimple>()
                .ForMember(rs => rs.Username, memberOptions => memberOptions.MapFrom(r => r.Account.UserName))
                .ForMember(rs => rs.AccountId, memberOptions => memberOptions.MapFrom(r => r.Account.Id));
            CreateMap<ResidentCreate, Resident>();
            CreateMap<ResidentUpdate, Resident>();

            CreateMap<AccountRegisterRequest, Account>();
            CreateMap<Account, AccountDetail>();
            CreateMap<Account, AccountSimple>();
        }
    }
}