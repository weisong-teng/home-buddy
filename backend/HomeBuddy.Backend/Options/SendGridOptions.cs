using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeBuddy.Backend.Options
{
    public class SendGridOptions
    {
        public string? SenderEmail { get; set; }
        public string? SenderName { get; set; }
        public string? ApiKey { get; set; }
        public string ConfirmPasswordResetTemplateId { get; set; } = "";
    }
}